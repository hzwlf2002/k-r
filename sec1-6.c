#include <stdio.h>
int main(){
	int c, i, nwhite, nother;
	nwhite = nother =0;
	int digit[10];
	for (i = 0; i < 10; i++){
		digit[i] = 0;
	}
	while ((c = getchar()) != EOF){
		if (c >= '0' && c <= '9'){
			++digit[c-'0'];
		}
		if (c == ' ' || c == '\t' || c == '\n'){
			++nwhite;
		}
		else {
			++nother;
		}
	}
	printf("\n");
	for(i = 0; i <= 9; i++){
		printf("digit[%d]: %-12d\n", i, digit[i]);
	}
	printf("nwhite: %-12d\nnother: %-12d\n", nwhite, nother);
}
