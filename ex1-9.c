#include <stdio.h>

int main(){
	int c, clast;
	clast = 'a';
	while ((c = getchar()) != EOF){
		if (clast == ' ' && c == ' ') {
			continue;
		}
		putchar(c);
		clast = c;
	}
	return 0;
}
