#include<stdio.h>
/* Print celsius when fahr = 300, 280, 260 ... 0
 * That is according to descend order
 */
int main(){
	int fahr;
	printf("fahr\tcelsius\n");
	for(fahr = 300; fahr >= 0; fahr = fahr - 20){
		printf("%4d\t%6.1f\n", fahr, (5.0/9.0)*(fahr-32));
	}
	return 0;
}
