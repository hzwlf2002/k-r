#include<stdio.h>
/* 当fahr = 0, 20, 40 ... 300时，分别
 * 打印华氏温度与摄氏温度的对照表
 */
int main(){
	float fahr, celsius;
	int lower, upper, step;
	lower = 0;
	upper = 300;
	step = 20;

	fahr = lower;
	printf("fahr\tcelsius\n");
	while (fahr <= upper){
		celsius = 5.0 * (fahr - 32) /9.0;
		printf("%4.0f\t%6.2f\n", fahr, celsius);
		fahr = fahr + 20;
	}
	return 0;
}
