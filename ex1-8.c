#include <stdio.h>

int main(){
	double ns, nt, nl;
	int c;
	nl = 0;
	while ((c = getchar()) != EOF) {
		if (c == ' ')
			ns++;
		if (c == '\t')
			nt++;
		if (c == '\n')
			nl++;
	}
	printf("ns: %0.f, nt: %0.f, nl: %0.f\n", ns, nt, nl);
	return 0;
}
