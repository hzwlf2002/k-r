#include<stdio.h>
/* 当fahr = 0, 20, 40 ... 300时，分别
 * 打印华氏温度与摄氏温度的对照表
 */
int main(){
	int fahr, celsius;
	int lower, upper, step;
	lower = 0;
	upper = 300;
	step = 20;

	fahr = lower;
	printf("fahr \t celsius\n");
	while (fahr <= upper){
		celsius = 5 * (fahr - 32) /9;
		printf("%d \t %d\n", fahr, celsius);
		fahr = fahr + 20;
	}
}
